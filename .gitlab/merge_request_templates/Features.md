## What does this MR do?


Guidelines for merging new features to master.


- [ ] Code compiled
- [ ] Local build successful
- [ ] PMD checks done
- [ ] Checkstyle run
- [ ] RTC Number Provided in description
- [ ] Reviewer 1
- [ ] Reviewer 2