package calculate;

/**
 * Created by abdulrafique on 10/11/2016.
 */
public interface Calculate {
    /**
     * Calculate a and b
     * @param a
     * @param b
     * @return
     */
    int calculate(int a,int b);
}
