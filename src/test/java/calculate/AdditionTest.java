package calculate;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by abdulrafique on 10/11/2016.
 */
public class AdditionTest {
    @Test
    public void calculate() throws Exception {
        Addition addition = new Addition();
        Assert.assertEquals(13, addition.calculate(10, 3));
    }

}